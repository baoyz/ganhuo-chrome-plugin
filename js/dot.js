$(document).ready(function(){
	AV.initialize("tcp0zp2t0lfvh8vf2g4x849hotv1rv1r5k2bilbrntsn3b90", "c96hgictontpudtxs2pwwr0hq7ykbvuymvnoyrh3uax4ggl2");
	var gh_modal;
	function gh_close_modal(){
		gh_modal.close();
		$("#gh_modal_wrapper").remove();
	}

	function submit_ganhuo(){
		var content = $("#gh_description").val();
		var url = window.location.url;
		var type = $("#gh_type").val();
		var who = $("#gh_who").val();

		if(!content || content.length == 0){
			alert('请填写描述语');
			return;
		}

		if(!who || who.length == 0){
			alert('请填写你的 ID');
			return;
		}

		var Ganhuo = AV.Object.extend("ganhuo");
		var ganhuo = new Ganhuo();
		ganhuo.save({
			url 	: 	window.location.href,
			desc : content,
			who  : who,
			type : type
		}, {
			success: function(object){
				gh_modal.close();
			}
		});
	}

	$(document.body).append('<button id="gh_collect" class="gh_circle">收藏</button>');
	$("#gh_collect").click(function(){
		var href = document.location.href;
		$.get(chrome.extension.getURL('modal.html'), function(data){
			$("body").append(data);
			gh_modal = new RModal(document.getElementById("gh_modal_wrapper"), {
		        beforeOpen: function(next) {
		          next();
		        },
		        afterOpen: function() {
	        		$('#gh_close').click(gh_close_modal);
	        		$('#gh_submit').click(submit_ganhuo);
	        		$("#gh_link").attr('href' ,window.location.href);
	        		$("#gh_link").html(window.location.href);
		        }
		        ,
		        beforeClose: function(next) {
		          next();
		        },
		        afterClose: function() {
		          	
		        },
		        dialogClass: "gh_modal",
		        bodyClass:"gh_modal_wrapper"
			});
			gh_modal.open();
		});
		
	})
});
